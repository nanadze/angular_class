/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ProductsListService } from './products-list.service';

describe('Service: ProductsList', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProductsListService]
    });
  });

  it('should ...', inject([ProductsListService], (service: ProductsListService) => {
    expect(service).toBeTruthy();
  }));
});
