import { Injectable } from '@angular/core';
import { AngularFire } from 'angularfire2';

@Injectable()
export class ProductsListService {
  productsObservable;

  getProducts(){
    this.productsObservable = this.af.database.list('/product');
    return this.productsObservable;
  }

  constructor(private af:AngularFire) { }

}
