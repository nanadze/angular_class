import { Component, OnInit } from '@angular/core';
import { ProductsListService } from './products-list.service';
import { AngularFire } from 'angularfire2';


@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {
  products;

  constructor(private _productsServise: ProductsListService) { }

  ngOnInit() {
    this._productsServise.getProducts()
    .subscribe(products =>
    {this.products = products});
    }
  }
