import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.service';
import { UserComponent } from './user/user.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PostsComponent } from './posts/posts.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { AngularFireModule } from 'angularfire2';
import { ProductsComponent } from './products/products.component';
import { ProductsService } from './products/products.service';
import { ProductComponent } from './product/product.component';
import { ProductsListComponent } from './products-list/products-list.component';
import { ProductsListService } from './products-list/products-list.service';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { InvoicesComponent } from './invoices/invoices.component';


 export const firebaseConfig = {
    apiKey: "AIzaSyB44rpVW5mfsVbDsm2gtVdMi28oe43cdB8",
    authDomain: "angular-a9358.firebaseapp.com",
    databaseURL: "https://angular-a9358.firebaseio.com",
    storageBucket: "angular-a9358.appspot.com",
    messagingSenderId: "242757054400"
 }

const appRoutes: Routes = [
  { path: 'users', component: UsersComponent },
  { path: 'posts', component: PostsComponent },
  { path: 'products', component:  ProductsComponent },
  { path: 'products-list', component: ProductsListComponent },
  { path: 'invoices', component: InvoicesComponent },
  { path: 'invoice-form', component: InvoiceFormComponent },
  { path: '', component: UsersComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    SpinnerComponent,
    PostsComponent,
    PageNotFoundComponent,
    UserFormComponent,
    ProductsComponent,
    ProductComponent,
    ProductsListComponent,
    InvoiceFormComponent,
    InvoicesComponent,
  ],

  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [UsersService, ProductsService, ProductsListService],
  bootstrap: [AppComponent]
})
export class AppModule { }
